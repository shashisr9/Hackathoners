package com.hcl.hackthon.chatapplication.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.hackthon.chatapplication.model.ChatMessageRequest;
import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;
import com.hcl.hackthon.chatapplication.model.ChatRooms;

import com.hcl.hackthon.chatapplication.service.ChatMessageService;

/*
 * ChatMessageController
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/chatapplication")
public class ChatMessageController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChatMessageController.class);

	@Autowired
	ChatMessageService chatMessageService;

	@RequestMapping(value = "/getchatroom", method = RequestMethod.GET, produces = "application/json")
	public List<String> getChatRooms() {

		LOGGER.info("..........getChatRooms...........");

		return chatMessageService.getAllChatRooms();

	}

	/*
	 * @RequestMapping(value = "/getchatroom1", method = RequestMethod.GET,
	 * produces = "application/json") public List<ChatRooms>
	 * getChatRoom1(@RequestBody ChatRooms chatroom) {
	 * 
	 * LOGGER.info("..........getChatRooms...........");
	 * 
	 * return chatMessageService.getAllChatRooms1(chatroom);
	 * 
	 * }
	 */

	/**
	 * 
	 * @param chatMessageRequest
	 * @return
	 */
	@RequestMapping(value = "/chatroom/{roomname}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public List<ChatMessageResponse> getMessgae(@RequestBody ChatMessageRequest chatMessageRequest) {

		return chatMessageService.saveChat(chatMessageRequest);

	}

	@RequestMapping(value = "/getchatdetails/{roomname}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	public List<ChatMessageResponse> getchatMessgae(@PathVariable("roomname") String roomname) {

		return chatMessageService.getChatDetails(roomname);

	}

	@RequestMapping(value = "/sendchat/{roomname}", method = RequestMethod.POST, produces = "application/json")
	public ChatMessageRequest addchat(@RequestBody ChatMessageRequest chatmessagerequest) {
		return chatMessageService.addchat(chatmessagerequest);
	}

	@RequestMapping(value = "/chatroom/{roomName}", method = RequestMethod.GET, produces = "application/json")

	public List<ChatMessageResponse> getChatRoomMessage(@PathParam(value = "roomName") String room) {

		LOGGER.info("..........getChatRooms...........");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();

		List<ChatMessageResponse> chatMessageResponses = new ArrayList<ChatMessageResponse>();
		ChatMessageResponse chatMessageResponse = new ChatMessageResponse();
		chatMessageResponse.setSenderName("user1");
		chatMessageResponse.setChatRoom("web devlopment");
		chatMessageResponse.setChatMessage("hello");
		chatMessageResponse.setDateTime(now.toString());
		chatMessageResponses.add(chatMessageResponse);

		ChatMessageResponse chatMessageResponse1 = new ChatMessageResponse();
		chatMessageResponse1.setSenderName("user2");
		chatMessageResponse1.setChatRoom("web devlopment");
		chatMessageResponse1.setChatMessage("hello");
		chatMessageResponse1.setDateTime(now.toString());
		chatMessageResponses.add(chatMessageResponse1);

		return chatMessageResponses;

	}

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String getHello() {

		return "hello world";

	}

}
