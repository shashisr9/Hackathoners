package com.hcl.hackthon.chatapplication.model;

/*
 * ChatMessageRequest
 */
public class ChatMessageResponse { 

	private String senderName;
	private String chatRoom;
	private String chatMessage;
	private String dateTime;

	/**
	 * @return the datetime
	 */
	public String getDateTime() {
		return dateTime;
	}

	/**
	 * @param datetime the datetime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @return the senderName
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @param senderName
	 *            the senderName to set
	 */
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	/**
	 * @return the chatRoom
	 */
	public String getChatRoom() {
		return chatRoom;
	}

	/**
	 * @param chatRoom
	 *            the chatRoom to set
	 */
	public void setChatRoom(String chatRoom) {
		this.chatRoom = chatRoom;
	}

	/**
	 * @return the chatMessage
	 */
	public String getChatMessage() {
		return chatMessage;
	}

	/**
	 * @param chatMessage
	 *            the chatMessage to set
	 */
	public void setChatMessage(String chatMessage) {
		this.chatMessage = chatMessage;
	}

}
