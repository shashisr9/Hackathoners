/**
 * 
 */
package com.hcl.hackthon.chatapplication.dao;

import java.util.List;

import com.hcl.hackthon.chatapplication.model.ChatMessageRequest;
import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;
import com.hcl.hackthon.chatapplication.model.ChatRooms;
import com.hcl.hackthon.chatapplication.model.UserDetails;

/**
 * @author Hackathon
 *
 */
public interface ChatMessageDAO {
	
	public List<String> getChatRooms();

	public List<ChatMessageResponse> saveChat(ChatMessageRequest chatMessageRequest);

	public List<UserDetails> saveUser(UserDetails userdetails);

	public List<ChatMessageResponse> getChatDetails(String roomname);

	public ChatMessageRequest addchat(ChatMessageRequest chatmessagerequest);

  //public List<ChatRooms> getChatRooms1(ChatRooms chatroom);

}
