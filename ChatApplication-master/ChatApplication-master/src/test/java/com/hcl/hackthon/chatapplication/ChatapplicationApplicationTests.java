package com.hcl.hackthon.chatapplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatapplicationApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void createChatRoomTest() {
		ChatMessageResponse chatMessageResponse = new ChatMessageResponse();
		chatMessageResponse.setSenderName("user1");
		chatMessageResponse.setChatRoom("web devlopment");
		chatMessageResponse.setChatMessage("hello");
		chatMessageResponse.setDateTime("2017-06-15 00:00:00");
		
	}

}
