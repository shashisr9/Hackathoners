﻿(function module(window, angular) {
    var linusapp = angular.module('linusApp', ['ngRoute']);
    linusapp.config(function ($routeProvider) {
        $routeProvider
            .when("/login", {
                templateUrl: "views/login.html",
                controller:"loginController"
            })
            .when("/chatrooms", {
                templateUrl: "views/chatrooms.html",
                controller: "chatRoomController"
            })
            .when("/chatbox", {
                templateUrl: "views/chatbox.html",
                controller: "chatBoxController"
            })
            .otherwise({
                redirectTo: '/login'
            });
    });
    window.linusapp = linusapp;
})(window, angular);