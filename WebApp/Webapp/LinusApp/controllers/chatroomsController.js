﻿(function loginCtrl() {
    linusapp.controller('chatRoomController', ['$scope', '$location', function ($scope, $location) {
        $scope.chatRooms = [
            {
                roomName:"Developer Hub"
            },
            {
                roomName: "Testing Hub"
            },

            {
                roomName: "Deployer Hub"
            }
        ];
        $scope.goToChatBox = function () {
            $location.path('/chatbox')
        }
    }]);
})();