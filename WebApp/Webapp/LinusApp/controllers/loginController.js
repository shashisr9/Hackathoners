﻿(function loginCtrl() {
    linusapp.controller('loginController', ['$scope', '$location', function ($scope, $location) {
        $scope.userName = '';
        $scope.goToRooms = function () {
            window.localStorage.setItem('userName', $scope.userName);
            $location.path('/chatrooms')
        }
    }]);
})();