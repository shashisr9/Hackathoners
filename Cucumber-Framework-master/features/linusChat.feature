Feature: As a Linus user, I would like to choose a chat room so my conversation is specific to
  my interests.

  Scenario: User Navigates to join Group Page
    Given When I am on landing page
    When I enter user name and Hit enter
    Then I See user created and chat group page with avilable Group List

  Scenario Outline: User can joining chat room
    Given When I am on landing page
    When I joined Group chat "<group>" as "<User>"
    Then I Should be successfully join "<group>" chat group

    Examples: 
      | group | User  |
      | abc   | user1 |
      | xyz   | user2 |

  Scenario: User Post Message in chat group
    Given I am on Chat Group "<group>" page as "<User1>"
    When I compose message
    And I click on post button
    Then I See my message is posted
    And I see message is labelled with username

      Scenario: User can see messages posted by others
    Given I am on Chat Group "group1" page as "User1"
    And  I post message
    When I am on Chat Group "group1" page as "User2"
    Then I See  message posted by "user1"
    
    Scenario: User can delete messages
    Given I am on Chat Group "group1" page as "User1"
    When  I post message
    Then I dont see option to edit existing message

    
    Scenario: User can search messages
    Given I am on Chat Group "group1" page as "User1"
    When  I perform search operation using message subject
    Then I see  results matching search criteria
    
    
    