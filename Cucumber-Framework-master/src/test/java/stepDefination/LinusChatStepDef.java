package stepDefination;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import proporties.ConfigReader;

public class LinusChatStepDef {

	WebDriver driver;
	
	ConfigReader con = new ConfigReader();

	
	@Given("^When I am on landing page$")
	public void when_I_am_on_landing_page() throws Throwable {
				driver.get(con.getApplicationUrl());
	}

	@When("^I enter user name and Hit enter$")
	public void i_enter_user_name_and_Hit_enter() throws Throwable {
	}

	@Then("^I See user created and chat group page with avilable Group List$")
	public void i_See_user_created_and_chat_group_page_with_avilable_Group_List() throws Throwable {
	}

	@When("^I joined Group chat \"(.*?)\" as \"(.*?)\"$")
	public void i_joined_Group_chat_as(String arg1, String arg2) throws Throwable {
	}

	@Then("^I Should be successfully join \"(.*?)\" chat group$")
	public void i_Should_be_successfully_join_chat_group(String arg1) throws Throwable {
	}

	// @Given("^I am on Chat Group \"(.*?)\" page as \"(.*?)\"$")
	// public void i_am_on_Chat_Group_page_as(String arg1, String arg2) throws
	// Throwable {
	// }

	@When("^I compose message$")
	public void i_compose_message() throws Throwable {
	}

	@When("^I click on post button$")
	public void i_click_on_post_button() throws Throwable {
	}

	@Then("^I See my message is posted$")
	public void i_See_my_message_is_posted() throws Throwable {
	}

	@Then("^I see message is labelled with username$")
	public void i_see_message_is_labelled_with_username() throws Throwable {
	}

	// @When("^I am on Chat Group \"(.*?)\" page as \"(.*?)\"$")
	// public void i_am_on_Chat_Group_page_as(String arg1, String arg2) throws
	// Throwable {
	// // Write code here that turns the phrase above into concrete actions
	// throw new PendingException();
	// }

	@Then("^I See  message posted by \"(.*?)\"$")
	public void i_See_message_posted_by(String arg1) throws Throwable {
		// // Write code here that turns the phrase above into concrete actions
		// throw new PendingException();
	}

	@When("^I post message$")
	public void i_post_message() throws Throwable {
	}

	@Then("^I dont see option to edit existing message$")
	public void i_dont_see_option_to_edit_existing_message() throws Throwable {
	}

	@When("^I perform search operation using message subject$")
	public void i_perform_search_operation_using_message_subject() throws Throwable {
	}

	@Then("^I see  results matching search criteria$")
	public void i_see_results_matching_search_criteria() throws Throwable {
	}

	@Given("^Open firefox and start application$")
	public void Open_firefox_and_start_application() throws Throwable {

		
	}

	@When("^I enter valid \"([^\"]*)\" and valid \"([^\"]*)\"$")
	public void I_enter_valid_username_and_valid_password(String uname, String pass) throws Throwable {

		driver.findElement(By.id("email")).sendKeys(uname);
		driver.findElement(By.id("pass")).sendKeys(pass);
	}

	@Then("^user should be able to login successfully$")
	public void user_should_be_able_to_login_successfully() throws Throwable {
		driver.findElement(By.id("loginbutton")).click();

	}

	@Then("^application should be closed$")
	public void application_should_be_closed() throws Throwable {
		driver.quit();
	}

	
	
}
