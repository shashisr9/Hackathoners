package commonUtils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import proporties.ConfigReader;

public class Hooks {

	WebDriver driver;
	ConfigReader con = new ConfigReader();

	@Before("@test")
	public void beforeScenario() {

		if (con.getBrowserType().toLowerCase() == "chrome") {
			System.setProperty("webdriver.chrome.driver", con.getDriverPath());
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			
		} else if (con.getBrowserType().toLowerCase() == "firefox") {

		}
	}

	@After("@test")
	public void afterScenario() {
		driver.quit();
	}

}
